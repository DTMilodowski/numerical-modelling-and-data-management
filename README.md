# README.md

This folder contains materials for the Numerical Modelling and Data Management (MDMG) Course.
This document is being used to keep track of what's in the folder.
*.md files should automatically be converted into HTML in the corresponding BitBucket repo.

## Contents

(1) A README file

(2) A folder with ice core data for plotting in the matplotlib and inkscape tutorial

(3) A python plotting script
